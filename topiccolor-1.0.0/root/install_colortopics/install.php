<?php
/** 
*
* @package installation file
* @version $Id: install.php , v.1.0.0 2012 $
* @author 2012 Daniel Heyne < http://topdan.de > < daniel@topdan.de >
* @copyright (c) 2012 Daniel Heyne < http://topdan.de > < daniel@topdan.de >
*
*/
	define("IN_PHPBB", true);
	$phpbb_root_path = "../";
	$phpEx = substr(strrchr(__FILE__, "."), 1);
	include("{$phpbb_root_path}common.{$phpEx}");
	
	if ($db->sql_query("INSERT INTO `phpbb_config` (`config_name`, `config_value`, `is_dynamic`) VALUES ('colortopics_color_0', '0', 0), ('colortopics_color_1', '0', 0), ('colortopics_color_2', '0', 0), ('colortopics_color_3', '0', 0), ('colortopics_active', '0', 0)"))
		trigger_error("Die Installation der Modifikation ist erfolgreich abgeschlossen worden.");
?>