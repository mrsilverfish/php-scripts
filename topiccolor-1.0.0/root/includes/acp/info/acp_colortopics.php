<?php
/** 
*
* @package language file
* @version $Id: acp_colortopics.php , v.1.0.0 2012 $
* @author 2012 Daniel Heyne < http://topdan.de > < daniel@topdan.de >
* @copyright (c) 2012 Daniel Heyne < http://topdan.de > < daniel@topdan.de >
*
*/

/**
* @package module_install
*/
class acp_colortopics_info
{
	function module()
	{
		return array(
			'filename'	=> 'acp_colortopics',
			'title'		=> 'ACP_COLORTOPICS',
			'version'	=> '1.0.0',
			'modes'		=> array(
				'main'		=> array(
					'title' => 'ACP_COLORTOPICS',
					'auth' => '',
					'cat' => array('ACP_COLORTOPICS')
				)
			),
		);
	}

	function install()
	{
	
	}

	function uninstall()
	{
	
	}
}


?>