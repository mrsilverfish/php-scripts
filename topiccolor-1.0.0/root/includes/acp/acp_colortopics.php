<?php
/** 
*
* @package acp
* @version $Id: acp_colortopics.php , v.1.0.0 2012 $
* @author 2012 Daniel Heyne < http://topdan.de > < daniel@topdan.de >
* @copyright (c) 2012 Daniel Heyne < http://topdan.de > < daniel@topdan.de >
*
*/

if (!defined('IN_PHPBB'))
	exit();

class acp_colortopics
{
	var $u_action;
	
	function main($id, $mode)
	{
		global $phpbb_root_path, $phpEx, $template, $db, $user;
		
		$user->add_lang("acp/colortopics");
		require($phpbb_root_path."includes/class_colortopics.".$phpEx);
		
		/* ===== VARS ===== */
		$submit = $_POST["main_submit"];
		$colortopics = new colortopics;
		
		$this->tpl_name = 'acp_colortopics';
		$this->page_title = 'ACP_COLORTOPICS';
		
		if (isset($submit))
		{
			$mod_active = request_var("mod_active", 0);
			$color0 = request_var("topiccolor_0", "");
			$color1 = request_var("topiccolor_1", "");
			$color2 = request_var("topiccolor_2", "");
			$color3 = request_var("topiccolor_3", "");
			
			$colortopics->update_cconfig("colortopics_active", $mod_active);
			$colortopics->update_cconfig("colortopics_color_0", $color0);
			$colortopics->update_cconfig("colortopics_color_1", $color1);
			$colortopics->update_cconfig("colortopics_color_2", $color2);
			$colortopics->update_cconfig("colortopics_color_3", $color3);

			trigger_error($user->lang['ACP_COLORTOPICS_UPDATE'].adm_back_link($this->u_action));
		}
		
		$template->assign_vars(array(
			"MOD_ACTIVE" => $colortopics->get_cconfig("colortopics_active"),
			"TOPICCOLOR_0" => $colortopics->get_cconfig("colortopics_color_0"),
			"TOPICCOLOR_1" => $colortopics->get_cconfig("colortopics_color_1"),
			"TOPICCOLOR_2" => $colortopics->get_cconfig("colortopics_color_2"),
			"TOPICCOLOR_3" => $colortopics->get_cconfig("colortopics_color_3"),
		));
	}
	
	function install()
	{
		/* NO FUNCTION DATA */
	}
	
	function uninstall()
	{
		/* NO FUNCTION DATA */
	}
}
?>