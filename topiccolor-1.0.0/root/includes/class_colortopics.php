<?php
/** 
*
* @package colortopics
* @version $Id: class_colortopics.php , v.1.0.0 2012 $
* @author 2012 Daniel Heyne < http://topdan.de > < daniel@topdan.de >
* @copyright (c) 2012 Daniel Heyne < http://topdan.de > < daniel@topdan.de >
*
*/
	class colortopics
	{
		/**
		 * Update an value of the phpBB3 config table
		 *
		 * @var string $name Name of the config row
		 * @var string $value Value for the config row
		 */
		function update_cconfig($name, $value)
		{
			global $db, $table_prefix;
			
			$db->sql_query("UPDATE `{$table_prefix}config` SET `config_value` = '{$db->sql_escape($value)}' WHERE `config_name` = '{$name}' LIMIT 1");
		}
		
		/**
		 * Gets the value for an specified field of the phpBB3 config table
		 *
		 * @var string $name Name of the config row
		 */
		function get_cconfig($name)
		{
			global $db, $table_prefix;
			
			$query = $db->sql_query("SELECT `config_value` FROM `{$table_prefix}config` WHERE `config_name` = '{$db->sql_escape($name)}'");
			$value = $db->sql_fetchfield("config_value");
			
			return $value;
		}
		
		/**
		 * Returns the style for an phpBB topictype
		 *
		 * @var integer $topictype
		 */
		function get_style($topictype)
		{
			$style = $this->get_cconfig("colortopics_color_{$topictype}");
			
			if ($style != "0")
				return $style;
			else
				return false;
		}
	}
?>