<?php
/** 
*
* @package language file
* @version $Id: colortopics.php , v.1.0.0 2012 $
* @author 2012 Daniel Heyne < http://topdan.de > < daniel@topdan.de >
* @copyright (c) 2012 Daniel Heyne < http://topdan.de > < daniel@topdan.de >
*
*/

if (!defined('IN_PHPBB'))
	exit;

if (empty($lang) || !is_array($lang))
	$lang = array();

$lang = array_merge($lang, array(
	"ACP_COLORTOPICS" => "Themen färben",
	"ACP_COLORTOPICS_UPDATE" => "Die Einstellungen wurden erfolgreich gespeichert.",
	"ACP_COLORTOPICS_EXPLAIN" => "Hier können sie bestimmte Thementypen in Foren färben lassen.",
	"ACP_COLORTOPICS_ACTIVE" => "Themen färben aktivieren",
	"ACP_COLORTOPICS_ACTIVE_EXPLAIN" => 'Mit dieser Einstellung werden die Thementypen mit einer von dir bestimmten Farbe hinterlegt. ',
	"ACP_COLORTOPICS_TYPE_0" => "Themen-Typ 0 - Standart-Themen",
	"ACP_COLORTOPICS_TYPE_1" => "Themen-Typ 1 - Wichtige-Themen",
	"ACP_COLORTOPICS_TYPE_2" => "Themen-Typ 2 - Bekanntmachungen",
	"ACP_COLORTOPICS_TYPE_3" => "Themen-Typ 3 - Globale Bekanntmachungen",
	"ACP_COLORTOPICS_TYPE_EXPLAIN" => "Geben sie hier einen CSS Style an, mit dem die Themen dieses Types gefärbt werden sollen. Geben sie 0 an, um diese Einstellung zu deaktivieren."
));
?>