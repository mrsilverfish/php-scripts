--
-- Table Structure for `polls`
--

CREATE TABLE IF NOT EXISTS `polls` (
  `poll_name` varchar(20) NOT NULL,
  `poll_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `poll_value` varchar(20) NOT NULL,
  PRIMARY KEY (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table Structure for `polls_results`
--

CREATE TABLE IF NOT EXISTS `polls_results` (
  `result_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `result_pollname` varchar(20) NOT NULL,
  `result_pollid` mediumint(8) NOT NULL,
  `result_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`result_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
