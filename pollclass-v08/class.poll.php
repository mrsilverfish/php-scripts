<?php
/**
 * This class contains all poll actions
 *
 * @author Daniel Heyne <http://dev.topdan.de/>
 * @copyright (c) Daniel Heyne, 2013
 */
	class poll
	{
		/**
		 * an array filled with all poll options
		 * @var array
		 */
		private $pollary = array();
		
		/**
		 * the variable contains the name of the poll
		 * @var string
		 */
		private $pollname = "";
		
		/**
		 * the variable contains the question of the poll
		 * @var string
		 */
		private $question = "";
		
		/**
		 * CHANGE ONLY, IF THE FILENAME OF THE POLL-SCRIPT IS DIFFERENT TO THE STANDARD
		 * the name of the file which contains the poll-script
		 * @var string
		 */
		private $filename = "index.php";
		
		function __construct($pollname, $question, $filename = false)
		{
			$this->pollname = $pollname;
			$this->question = $question;
			
			if ($filename)
			{
				$filename = pathinfo($filename);
				$this->filename = $filename["basename"];
			}
		}
		
		/**
		 * initalisation of the poll-script
		 */
		function init()
		{
			if ($this->pollname == "" || empty($this->pollname))
				die("Poll Error: No Pollname<br />Script end;");
			
			$query = mysql_query("SELECT * FROM `".POLLTABLE."` WHERE `poll_name` = '{$this->pollname}'");
			$count = mysql_num_rows($query);
			
			if ($count == 0)
				die("Poll Error: Can't find Polldata<br />Script end;");
			
			while ($row = mysql_fetch_object($query))
			{
				$this->pollary[] = array(
					"selectid" => $row->poll_id,
					"select_name" => $row->poll_value
				);
			}
		}
		
		/**
		 * function to display the poll
		 *
		 * @echo poll and poll-form
		 */
		function display()
		{
			if (!is_array($this->pollary))
				return false;
			if (count($this->pollary) <= 1)
				return false;
			
			$query = mysql_query("SELECT * FROM `".POLLTABLE_RESULT."` WHERE `result_pollname` = '{$this->pollname}'");
			$count = mysql_num_rows($query);
			$polls = '<form action="'.$this->filename.'" method="post">';
			$polls .= "<p>{$this->question}</p>";
			
			for ($i = 0; $i < count($this->pollary); $i++)
			{
				$counter = mysql_num_rows(mysql_query("SELECT * FROM `".POLLTABLE_RESULT."` WHERE `result_pollname` = '{$this->pollname}' AND `result_pollid` = {$this->pollary[$i]["selectid"]}"));
				
				$result = @ceil(100 * $counter / $count);
				if (VOTE_RESULT == "percent")
					$endresult = $result.'%';
				else if (VOTE_RESULT == "users")
					$endresult = $counter.' / '.$count;
				$polls .= '<label><input type="radio" name="pollid" value="'.$this->pollary[$i]["selectid"].'" />'.$this->pollary[$i]["select_name"].'</label> ['.$endresult.']';
				
				if ($i < (count($this->pollary) - 1))
					$polls .= "<br />";
			}
			$polls .= '<p><input type="submit" name="submit" value="Vote" /></p>';
			$polls .= "</form>";
			
			echo $polls;
		}
		
		/**
		 * function to vote to a specific poll
		 *
		 * @var integer $pollid
		 */
		function vote($pollid)
		{
			global $_POST;
			
			$pollid = mysql_real_escape_string($pollid);
			
			if (isset($_POST["pollid"]))
			{
				$ip = $_SERVER["REMOTE_ADDR"];
				$query = mysql_query("SELECT `result_pollid` FROM `".POLLTABLE_RESULT."` WHERE `result_ip` = '$ip' AND `result_pollname` = '{$this->pollname}' LIMIT 1");
				
				$mysql_true = false;
				if (mysql_num_rows($query) > 0)
				{
					if (mysql_query("UPDATE `".POLLTABLE_RESULT."` SET `result_pollid` = {$pollid} WHERE `result_ip` = '$ip' AND `result_pollname` = '{$this->pollname}' LIMIT 1"))
						$mysql_true = true;
				}
				else
				{
					if (mysql_query("INSERT INTO `".POLLTABLE_RESULT."` VALUES (NULL, '{$this->pollname}', {$pollid}, '$ip')"))
						$mysql_true = true;
				}
				
				if (!$mysql_true)
					die("MySQL Error: ".mysql_error()." [".mysql_errno()."]");
				
				header("Location: ./{$this->filename}");
			}
		}
		
		/**
		 * function to check to new versions of the poll class
		 * ATTENTION: Versionserver ist down! No functionality.
		 */
		function pollclass_version($version_check = false, $abbr = false)
		{
			$version_info = file_get_contents("http://versions.topdan.de/?product=pollclass".((!$abbr) ? "&handler=on" : ""));
						
			if (!$version_check)
				echo $version_info;
			else
			{
				echo "<p>Class-Info: ";
				if ($version_info == CLASS_VERSION)
					echo "The current Classversion ist the latest.</p>";
				else
					echo 'There are new Versions on TopDan: <a href="http://topdan.de/!/page_products/pollclass">Latest Class</a></p>';
			}
		}
	}
?>