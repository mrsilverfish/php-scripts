<?php
	$dbc = array();
	
	/*
	 * MySQL Host
	 */
	$dbc["host"] = "localhost";
	
	/*
	 * MySQL Username
	 */
	$dbc["user"] = "root";
	
	/*
	 * MySQL User Password
	 */
	$dbc["pass"] = "";
	
	/*
	 * MySQL Database Name
	 */
	$dbc["name"] = "pollclass";
	
	$link = mysql_connect($dbc["host"], $dbc["user"], $dbc["pass"]);
	mysql_select_db($dbc["name"], $link);
	
	define("POLLTABLE", "polls");
	define("POLLTABLE_RESULT", "polls_results");
	
	/*
	 * The Vote Results
	 * @params percent, users
	 */
	define("VOTE_RESULT", "users");
	
	
	/*
	 * ---------------------------------------------
	 * Don't Change anything form this point.
	 * ---------------------------------------------
	 */
	define("CLASS_VERSION", "0.8");
?>